This license applies to all files containing code or assets under the Sirena namespace; as well as fragmented modifications (i.e. commits) made by the Sirena project on top of the upstream Corvax Space Station 14 source code (https://github.com/space-syndicate/space-station-14) if not attributed otherwise in the modified files, or their containing folders' attributions.yml, licenses.txt, meta.json or other in-folder license-specifying files.

GNU GENERAL PUBLIC LICENSE
Version 2, June 1991

Copyright (c) 2023 Sirena

Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.


Additional Terms:

By using this software, you agree to the following additional terms:
1. If you use any code from this repository in your own project, you are required to make the source code of your project available under the terms of the GNU General Public License, Version 2.0.


Disclaimer of Warranty:

The software is provided "as is," without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. In no event shall the authors or copyright holders be liable for any claim, damages, or other liability, whether in an action of contract, tort, or otherwise, arising from, out of, or in connection with the software or the use or other dealings in the software.
