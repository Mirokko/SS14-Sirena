ent-BlueSpaceCrystalOre = неочищенный блюспейс кристалл
    .desc = Очень дорогой и редкий материал используемый для блюспейс технологий. К сожалению он ещё не обработан
    .suffix = { "Полный" }

ent-BlueSpaceCrystalCut = блюспейс кристалл
    .desc = Очень дорогой и редкий материал используемый для блюспейс технологий.
    .suffix = { "Полный" }

ent-BlueSpaceCrystalOre1 = { ent-BlueSpaceCrystalOre }
    .desc = { ent-BlueSpaceCrystalOre.desc }
    .suffix = { "Один" }

ent-BlueSpaceCrystalCut1 = { ent-BlueSpaceCrystalCut }
    .desc = { ent-BlueSpaceCrystalCut.desc }
    .suffix = { "Один" }

ent-BlueSpaceOreBag =  блюспейс сумка для руды
    .desc = Специальное хранилище для руды. Для повышения вместимости использована блюспейс технология.

ent-BlueSpacePillCanister = блюспейс баночка для таблеток
    .desc =  Специальное хранилище для таблеток. Для повышения вместимости использована блюспейс технология.

ent-BluespaceAirTank = блюспейс воздушный баллон
    .desc = Вы уверены что вам этого хватит?

ent-WallRockBluespace = { ent-WallRock }
    .desc = Рудная жила, богатая блюспейс кристаллами.
    .suffix = Блюспейс кристаллы

ent-WallRockBasaltBluespace = { ent-WallRockBasalt }
    .desc = { ent-WallRockBluespace.desc }
    .suffix = Блюспейс кристаллы

ent-WallRockSnowBluespace = { ent-WallRockSnow }
    .desc = { ent-WallRockBluespace.desc }
    .suffix = Блюспейс кристаллы
