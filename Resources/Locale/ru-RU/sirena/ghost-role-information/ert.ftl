ghost-role-information-cburnunitleadersirena-name = лидер отряда ОБХАЗ
ghost-role-information-cburnunitleadersirena-description = Лидер отряда ОБХАЗ. Повенуется приказам с ЦК.

ghost-role-information-cburnunitsirena-name = член отряда ОБХАЗ
ghost-role-information-cburnunitsirena-description = Выполняйте приказы вашего лидера!


ghost-role-information-deadsquadsirena-name = член отряда эскадрона смерти
ghost-role-information-deadsquadsirena-description = Ождайте приказа от ЦК...


ghost-role-information-ertleadersirena-name = лидер ОБР
ghost-role-information-ertleadersirena-description = Лидер отряда быстрого реагирования. Повенуется приказам с ЦК.

ghost-role-information-ertengineersirena-name = инженер ОБР
ghost-role-information-ertengineersirena-description = Инженер отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertjanitorsirena-name = уборщик ОБР
ghost-role-information-ertjanitorsirena-description = Уборщик отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertmedicalsirena-name = медик ОБР
ghost-role-information-ertmedicalsirena-description = Медик отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!

ghost-role-information-ertsecuritysirena-name = СБ ОБР
ghost-role-information-ertsecuritysirena-description = Служба безопасности отряда быстрого реагирования. Ваша первостепенная задача - выполнение приказов вашего лидера!
